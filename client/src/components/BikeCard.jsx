import { Button, Card, CardActions, CardContent, CardMedia, Checkbox, FormControlLabel, FormGroup, Typography } from "@mui/material"
import { useEffect, useState } from "react"
import { UserService } from "../service/UserService";
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import EditIcon from '@mui/icons-material/Edit';
import { useNavigate } from "react-router-dom";
import { BikeService } from "../service/BikeService";
import { toast } from "react-toastify";
export const BikeCard = (item) => {
    const navigate = useNavigate();

    function handleEdit(id) {
        navigate(`/editbike/${id}`)
    }

    function handleDelete(id) {
        BikeService.DeleteBike()
            .then(()=> {
                toast.success("Bike deleted successfully!")})
            .catch((err) => {
                toast.error(err.message)
            })
    }
    const { model, color, location, rating, isAvailable, id: _id } = item.item
    return (
        <div className="bike-card-div">
            <Card sx={{ maxWidth: 345, display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center" }}>
                <CardMedia
                    component="img"
                    height="140"
                    src={require("../assests/gixer.jpeg")}
                    alt="green iguana"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        {model}
                    </Typography>

                    <Typography variant="body2" color="text.secondary">
                        {color}
                    </Typography>

                    <Typography variant="body2" color="text.secondary">
                        {location}
                    </Typography>

                    <Typography variant="body2" color="text.secondary">
                        {rating}
                    </Typography>



                </CardContent>
                <CardActions>
                    <EditIcon onClick={() => handleEdit(item.id)} color="action" />
                    <Button size="small">Book</Button>
                    <DeleteOutlineIcon onClick={() => handleDelete(item.id)} size="small" color="warning" />
                </CardActions>
            </Card>
            <br />
        </div>
    )
}