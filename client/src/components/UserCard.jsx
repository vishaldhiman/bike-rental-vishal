import { Button, Card, CardActions, CardContent, CardMedia, Checkbox, FormControlLabel, FormGroup, Typography } from "@mui/material"
import { useEffect, useState } from "react"
import { UserService } from "../service/UserService";
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import EditIcon from '@mui/icons-material/Edit';
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
export const UserCard = ({item}) => {
    console.log(item)
    const navigate = useNavigate();

    function handleEditUser(id) {
        navigate(`/edituser/${id}`)
    }

    function handleDeleteUser(id) {
        UserService.deleteUser()
            .then(()=> {
                toast.success("User deleted successfully!")})
            .catch((err) => {
                toast.error(err.message)
            })
    }

    return (
        
            <Card sx={{ maxWidth: 345, display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center" }}>
                <CardMedia
                    component="img"
                    className="user-image"
                    src={require("../assests/avatar7.png")}
                    alt="green iguana"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        Name : {item.username}
                    </Typography>

                    <Typography variant="body2" color="text.secondary">
                        Email : {item.email}
                    </Typography>

                    <Typography variant="body2" color="text.secondary">
                        Role: {item.role}
                    </Typography>



                </CardContent>
                    <div>
                    <EditIcon style={{marginRight:"250px"}} color="action" onClick = { () => {handleEditUser(item._id)}}/>
                    <DeleteOutlineIcon onClick = { () => {handleDeleteUser(item._id)}} size="small" color="warning" />
                    </div>
            </Card>
        
    )
}