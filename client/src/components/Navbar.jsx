import React, { useContext } from "react";
import { Link, useNavigate } from "react-router-dom";
import { Button } from "@mui/material";
import { toast } from "react-toastify";
import { UserContext } from "../context/UserContext";
function Navbar() {
    const { user, setUser } = useContext(UserContext);
    const navigate = useNavigate();
    function handleLogStatus() {
        setUser(null);  
        toast.success("Logged out successfully!");
        navigate('/login')
    }

    return (
        <div className="navbar-div">
            <div>
                <Link className="nav-buttons" to="/">
                    Home
                </Link>
                <Link className="nav-buttons" to="/users">
                    Users
                </Link>

                <Link className="nav-buttons" to="/myevents">
                    Bookings
                </Link>
            </div>

            <div>
                <Button onClick={handleLogStatus}>
                    {user.role?"Logout":"Login"}
                </Button>
            </div>
        </div>
    );
}

export default Navbar;