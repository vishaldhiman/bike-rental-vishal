import logo from './logo.svg';
import './App.css';
import { Route, Routes } from 'react-router';
import { Signup } from './pages/Signup';
import Login from './pages/Login';
import { Home } from './pages/Home';
import { UserBookings } from './pages/UserBookings';
import { BikeBookings } from './pages/BikeBookings';
import { AddBike } from './pages/AddBike';
import { DisplayUsers } from './pages/DisplayUsers';
import { EditBike } from './pages/EditBike';
import { ToastContainer } from 'react-toastify';
import { useEffect, useState } from 'react';
import { useCookies } from 'react-cookie';
import { UserContext } from './context/UserContext';
import { EditUser } from './pages/EditUser';

function App() {
  const [user, setUser] = useState(null);
  const [cookies, setCookie, removeCookie] = useCookies(["token", "user"])

  useEffect(() => {
    if (cookies.user) setUser(cookies.user)
  }, [cookies.user])
  return (
    <UserContext.Provider value={{ user, setUser }}>
      <div className="App">
        <Routes>
          {!user && (
            <>
              <Route path='/signup' element={<Signup />} />
              <Route path='/login' element={<Login />} />
              <Route path='*' element={<Login/>}/>
            </>
          )}
          {user && (
            <>
            <Route path='/userbookings/:id' element={<UserBookings />} />
            {user.role === "manager" && <Route path='/bikebookings/:id' element={<BikeBookings />} />}
            {user.role === "manager" && <Route path='/addevent' element={<AddBike />} />}
            {user.role === "manager" && <Route path='/editbike/:bikeid' element={<EditBike />} />}
            {user.role === "manager" && <Route path='/edituser/:userid' element={<EditUser/>} />}

            {user.role === "manager" && <Route path='/users' element={<DisplayUsers />} />}

            <Route path='*' element={<Home />} />
            </>
          )}
        </Routes>
      </div>
      <ToastContainer/>
    </UserContext.Provider>
  );
}

export default App;
