
export class BikeService {

    static async getBikes(payload={}) {
        const bikes = [
            {
                id: 1,
                model: "Suzuki xxr",
                color: "red",
                location: "Noida",
                rating: 4,
                iSAvailable: true
            },
            {
                id: 2,
                model: "Yamaha rtr",
                color: "blue",
                location: "Noida",
                rating: 4,
                iSAvailable: false
            },
            {
                id: 3,
                model: "Suzuki xxr",
                color: "red",
                location: "Noida",
                rating: 4,
                iSAvailable: false
            },
        ];
        return  {bikes,pages:1}
    }

    static async filterBikesData(payload) {
        const bikes = [
            {
                id: 1,
                model: "FZ-X",
                color: "orange",
                location: "Faridabad",
                rating: 3,
                iSAvailable: true
            },
            {
                id: 2,
                model: "Yamaha rtr",
                color: "blue",
                location: "Noida",
                rating: 4,
                iSAvailable: false
            },
        ]

        return {bikes,pages:1}
    }

    static async AddBike(payload) {
        const bikes = [];
        return bikes;

    }

    static async getSingleBike(id) {
        return []
    }

    static async updateSingleBike(payload) {
        console.log(payload)
        return payload;
    }

    static async DeleteBike(id) {
        return {res:"Deleted"}
    }
    
}