import { Button, Checkbox, FormControl, FormControlLabel, InputLabel, Select, TextField } from "@mui/material"
import { useEffect } from "react";
import { useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import Navbar from "../components/Navbar";
import { BikeService } from "../service/BikeService";

export const EditBike = () => {
    const params = useParams();
    const { bikeId } = params;
    const [model, setModel] = useState("");
    const [color, setColor] = useState("");
    const [location, setLocation] = useState("");
    const [rating, setRating] = useState(0);
    const [isAvailable, setIsAvailable] = useState(false);
    const navigate = useNavigate();

    useEffect(()=> {
        BikeService.getSingleBike(bikeId)
            .then(({model , location , color , isAvailable , rating}) => {
                setModel(model);
                setColor(color);
                setLocation(location)
                setIsAvailable(isAvailable);
                setRating(rating)
            })
    },[bikeId])

    const handleEventSubmit = () => {
        const payload = {model , location , color , isAvailable}
        BikeService.updateSingleBike(payload) 
            .then((data) => {
                console.log(data)
                toast.success("Bike updated successfully!");
                navigate('/')
            })
            .catch((err) => toast.error(err.message))
    }

    return (
        <>
            <Navbar />
            <div className="add-events-div">
                <i><h3>Add Bike</h3></i>
                <TextField
                    id="standard-basic"
                    label="Enter Bike Model"
                    variant="standard"
                    value={model}
                    onChange={(e) => setModel(e.target.value)}
                />
                <br />
                <TextField
                    id="standard-basic"
                    label="Enter Color"
                    variant="standard"
                    value={color}
                    onChange={(e) => setColor(e.target.value)}
                />
                <br />

                <TextField
                    id="standard-basic"
                    label="Enter Location"
                    variant="standard"
                    value={location}
                    onChange={(e) => setLocation(e.target.value)}
                />
                <br />

                <TextField
                    id="standard-basic"
                    label="Enter Rating"
                    variant="standard"
                    value={rating}
                    onChange={(e) => setRating(e.target.value)}
                />

                <br />


                <div style={{ marginTop: "20px" }}>
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={isAvailable}
                                onClick={(e) => setIsAvailable(!isAvailable)}
                            />
                        }
                        label="Availablity"
                    />
                    <Button variant="text" onClick={handleEventSubmit}>
                        Submit
                    </Button>
                    <Link to='/'>Cancel</Link>

                </div>
            </div>
        </>
    )
}