import React, { useContext, useState } from "react";
import {
    Button,
    Pagination,
} from "@mui/material";
import TextField from "@mui/material/TextField";
import "react-datepicker/dist/react-datepicker.css";
import { useEffect } from "react";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { toast } from "react-toastify";
import { BikeCard } from "../components/BikeCard";
import { BikeService } from "../service/BikeService";
import { UserContext } from "../context/UserContext";
import Navbar from "../components/Navbar";
import { queryPath } from "../utils/QueryPathGenerator";
import { useNavigate } from "react-router-dom";

export const Home = () => {
    const navigate = useNavigate();
    const { user, setUser } = useContext(UserContext);
    console.log(user)
    console.log("in home", { user });
    const [location, setLocation] = useState("");
    const [model, setModel] = useState("");
    const [color, setColor] = useState("");
    const [rating, setRating] = useState(0);
    const [startDate, setStartDate] = useState(null);
    const [endDate, setEndDate] = useState(null);
    const [page, setPage] = useState(1);
    const [isAvailable, setIsAvailable] = useState(true);
    const [bikesData, setBikesData] = useState([]);
    const [totalPage , setTotalPage] = useState(1);
    useEffect(() => {
        getAllBikes()
    }, [])

    const handleFilters = () => {
        const payload = {
            model,
            color,
            rating,
            startDate,
            endDate,
            isAvailable,
            page
        }
        const path = queryPath(payload);
        BikeService.filterBikesData(path)
            .then((data) => {
                setBikesData(data.bikes)
                setPage(1);
            })
            .catch((err) => {
                toast.error(err.message)
            })
    }

    function getAllBikes ()  {
        BikeService.getBikes()
        .then((data) => {
            setBikesData(data.bikes)
            // setPage(data.pages)
        })
        .catch((err) => {
            toast.error(err.message)
        })
    }

    const clearFeilds = () => {
        setModel("");
        setColor("");
        setLocation("");
        setRating("");
        setStartDate(null);
        setEndDate(null);
        setPage(1);
        getAllBikes();
    
    }

    return (
        <>
            <Navbar />
            <div className="filter-div">
                <TextField
                    onChange={(e) => setModel(e.target.value)}
                    id="outlined-basic"
                    label="Enter Model"
                    variant="outlined"
                    value={model}
                />
                <TextField
                    onChange={(e) => setColor(e.target.value)}
                    id="outlined-basic"
                    label="Enter Color"
                    variant="outlined"
                    value={color}
                />

                <TextField
                    onChange={(e) => setLocation(e.target.value)}
                    id="outlined-basic"
                    label="Enter Location"
                    variant="outlined"
                    value={location}
                />
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <DesktopDatePicker
                        label="Start-Date"
                        inputFormat="dd/MM/yyyy"
                        value={startDate}
                        onChange={(nd) => {

                            setStartDate(new Date(nd).toISOString());
                        }}
                        renderInput={(params) => <TextField {...params} />}
                    />
                    <DesktopDatePicker
                        label="End-Date"
                        value={endDate}
                        inputFormat="dd/MM/yyyy"
                        onChange={(nd) => {
                            setEndDate(new Date(nd).toISOString());
                        }}
                        renderInput={(params) => <TextField {...params} />}
                    />
                </LocalizationProvider>
               
            </div>
            <div style={{ marginBottom: "20px", padding: "10px" }}>
                <Button
                    variant="outlined"
                    style={{ marginRight: "10px", color: "green" }}
                    onClick={handleFilters}
                >
                    Submit
                </Button>

                <Button
                    variant="outlined"
                    style={{ color: "tomato" }}
                    onClick={clearFeilds}
                >
                    Clear
                </Button>
            </div>
            {user.role === "manager" && <Button onClick={()=> navigate('/addevent')} style={{marginBottom:"20px"}} variant="outlined">Add Event</Button>}
            <div className="homePage-div">
                <div className="bikes-cards-outer-div">
                    {bikesData.map((item) => {
                        return <BikeCard key={item.id} item={item} />
                    })}
                </div>
                <br />
                <Pagination style={{ marginBottom: "20px" }} count={totalPage} variant="outlined" shape="rounded" />
            </div>

        </>
    )
}