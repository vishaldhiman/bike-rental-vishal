import { useState } from "react"
import { BikeCard } from "../components/BikeCard";
const bikes = [
    {
        id: 1,
        model: "Suzuki xxr",
        color: "red",
        location: "Noida",
        rating: 4,
        iSAvailable: true
    },
    {
        id: 2,
        model: "Yamaha rtr",
        color: "blue",
        location: "Noida",
        rating: 4,
        iSAvailable: false
    },
    {
        id: 3,
        model: "Suzuki xxr",
        color: "red",
        location: "Noida",
        rating: 4,
        iSAvailable: false
    },
];
export const UserBookings = () => {
    const [data, setData] = useState(bikes);
    return (
        <div>
            {data.map((item) => {
                return <BikeCard key={item.id} item={item} />
            })}
        </div>
    )
}