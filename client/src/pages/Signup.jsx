import { Button, TextField } from "@mui/material";
import { useState } from "react";
import { useNavigate } from "react-router";
import { toast } from "react-toastify";
import { UserService } from "../service/UserService";

export const Signup = () => {
  const [username, setUserName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const navigate = useNavigate();
  const handleSubmit = async (event) => {
    if (password !== confirmPassword) toast.error("Confirm Password does not match with your password");
    
    if (username && email && password && confirmPassword) {
      const payload = {
        username,
        email,
        password,
        confirmPassword,
        role: "regular",
      };
      console.log(payload); 
      UserService.signupUser(payload)
        .then((data) => {
          console.log(data)
          toast.success("Signedup Successfully!")
          navigate('/login')
        })
        .catch((err) => {
          toast.success(err.message)

          console.log(err.message)
        })
    } else toast.error("PLease enter the textField");
  };

  return (
    <>
      <h2>Signup</h2>
      <div className="signup-input-container">
        <TextField
          id="outlined-basic"
          label="Enter username"
          variant="outlined"
          onChange={(e) => setUserName(e.target.value)}
        />
        <br />

        <TextField
          id="outlined-basic"
          label="Enter email"
          variant="outlined"
          onChange={(e) => setEmail(e.target.value)}
        />
        <br />
        <TextField
          id="outlined-basic"
          label="Enter password"
          variant="outlined"
          onChange={(e) => setPassword(e.target.value)}
        />
        <br />
        <TextField
          id="outlined-basic"
          label="Enter confirm password"
          variant="outlined"
          onChange={(e) => setConfirmPassword(e.target.value)}
        />
      </div>
      <Button onClick={handleSubmit} variant="outlined">
        Submit
      </Button>
    </>
  );
};
