import { useEffect } from "react";
import { useState } from "react"
import { toast } from "react-toastify";
import Navbar from "../components/Navbar";
import { UserCard } from "../components/UserCard";
import { UserService } from "../service/UserService";

export const DisplayUsers = () => {
    const [users,setUsers] = useState([]);

    useEffect(()=> {
        UserService.getAllUsers()
            .then((data) => {
                console.log(data)
                setUsers(data)
            })
            .catch((err) => {
                toast.error(err.message)
            })
    },[])
    return (
        <>
            <Navbar/>
        <div className="display-users-div">
            {users.map((user) => (
                <UserCard item={user} key={user._id}/>
            ))}
        </div>
        </>
    )
}