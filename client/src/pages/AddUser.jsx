import { Button, Checkbox, FormControl, FormControlLabel, InputLabel, Select, TextField } from "@mui/material"
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import Navbar from "../components/Navbar";
import { BikeService } from "../service/BikeService";
import { UserService } from "../service/UserService";

export const AddUser = () => {
    const [username, setUserName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const navigate = useNavigate();
    const handleEventSubmit = () => {
        const payload = {
            username,
            email,
            password
        }
        UserService.addUser(payload)
            .then((data) => {
                console.log("Data is", data)
                toast.success("Bike added successfully!")
                navigate('/')
            })
            .catch((err) => {
                toast.error(err.message)
            })
    }
    return (
        <>
            <Navbar />
            <div className="add-events-div">
                <i><h3>Add Event</h3></i>
                <TextField
                    id="standard-basic"
                    label="Enter Bike Model"
                    variant="standard"
                    value={model}
                    onChange={(e) => setModel(e.target.value)}
                />
                <br />
                <TextField
                    id="standard-basic"
                    label="Enter Color"
                    variant="standard"
                    value={color}
                    onChange={(e) => setColor(e.target.value)}
                />
                <br />

                <TextField
                    id="standard-basic"
                    label="Enter Location"
                    variant="standard"
                    value={location}
                    onChange={(e) => setLocation(e.target.value)}
                />
                <br />

                <TextField
                    id="standard-basic"
                    label="Enter Rating"
                    variant="standard"
                    value={rating}
                    onChange={(e) => setRating(e.target.value)}
                />

                <br />
                <div>
                    <LocalizationProvider dateAdapter={AdapterDateFns}>
                        <DesktopDatePicker
                            label="Start-Date"
                            inputFormat="dd/MM/yyyy"
                            value={startDate}
                            onChange={(nd) => setStartDate(nd)}
                            renderInput={(params) => <TextField {...params} />}
                        />
                        <br />
                        <br />
                        <DesktopDatePicker
                            label="End-Date"
                            inputFormat="dd/MM/yyyy"
                            value={endDate}
                            onChange={(nd) => setEndDate(nd)}
                            renderInput={(params) => <TextField {...params} />}
                        />
                    </LocalizationProvider>
                </div>


                <div style={{ marginTop: "20px" }}>
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={isAvailable}
                                onClick={(e) => setIsAvailable(!isAvailable)}
                            />
                        }
                        label="Availablity"
                    />
                    <Button variant="text" onClick={handleEventSubmit}>
                        Submit
                    </Button>

                </div>
            </div>
        </>
    )
}