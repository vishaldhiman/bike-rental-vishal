import React, { useContext, useState } from "react";
import TextField from "@mui/material/TextField";
import { Button } from "@mui/material";
import { useNavigate } from "react-router";
import { UserService } from "../service/UserService";
import { toast } from "react-toastify";
import {UserContext} from "../context/UserContext"
import { useCookies } from "react-cookie";
function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();
  const {user, setUser} = useContext(UserContext);
  const [cookies, setCookie] = useCookies(["token", "user"])
  const handleLogin = () => {

    const paylaod = {email,password};
        UserService.loginUser(paylaod)
          .then((data) => {
            console.log(data)
            setUser(data);
            setCookie("token",data.token);
            setCookie("user", data.user);
            navigate("/");
            toast.success("Loggedin Successfully!");
              console.log(data)
              console.log(cookies);
          })
          .catch((err) => {
            toast.success(err.message)
          })   
  };

  return (
    <div className="main-box">
      <div className="login-div">
        <h2>Login</h2>
        <TextField
          onChange={(e) => setEmail(e.target.value)}
          id="outlined-basic"
          label="Enter email"
          variant="outlined"
          fullWidth
        />
        <br />
        <TextField
          type="password"
          id="outlined-basic"
          label="Enter Password"
          variant="outlined"
          onChange={(e) => setPassword(e.target.value)}
          fullWidth
        />
        <br />
        <Button variant="contained" onClick={handleLogin}>
          Login
        </Button>
      </div>
    </div>
  );
}

export default Login;