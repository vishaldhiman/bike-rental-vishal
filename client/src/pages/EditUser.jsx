import { Button, Checkbox,  FormControlLabel, TextField } from "@mui/material"
import { useEffect } from "react";
import { useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import Navbar from "../components/Navbar";
import { UserService } from "../service/UserService";

export const EditUser = () => {
    const params = useParams();
    const { UserId } = params;
    const [name, setName] = useState("");
    const [email , setEmail] = useState("");
    const [role , setRole] = useState("");
    const navigate = useNavigate();

    useEffect(()=> {
        UserService.getSingleUser(UserId)
            .then(({name , email , role}) => {
                setName(name)
                setEmail(email);
                setRole(role);
            })
    },[UserId])

    const handleEventSubmit = () => {
        const payload= {name , email , role}
        UserService.updateSingleUser(payload) 
            .then((data) => {
                console.log(data)
                toast.success("User updated successfully!");
                navigate('/')
            })
            .catch((err) => toast.error(err.message))
    }

    return (
        <>
            <Navbar />
            <div className="add-events-div">
                <i><h3>Add User</h3></i>
                <TextField
                    id="standard-basic"
                    label="Enter User Name"
                    variant="standard"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                />
                <br />
                <TextField
                    id="standard-basic"
                    label="Enter Email"
                    variant="standard"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />
                <br />

                <TextField
                    id="standard-basic"
                    label="Enter Role"
                    variant="standard"
                    value={role}
                    onChange={(e) => setRole(e.target.value)}
                />
                <br />

                <div style={{ marginTop: "20px" }}>
                    <Button variant="text" onClick={handleEventSubmit}>
                        Submit
                    </Button>
                    <Link to='/users'>Cancel</Link>

                </div>
            </div>
        </>
    )
}